use clap::ValueEnum;

/// Graph databases
#[derive(Clone, Debug, PartialEq, ValueEnum)]
pub enum GraphDatabase {
    Neo4J,
    TigerGraph,
}

impl Default for GraphDatabase {
    fn default() -> Self {
        Self::Neo4J
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn graph_database() {
        let gdb = GraphDatabase::Neo4J;
        assert_eq!(GraphDatabase::Neo4J, gdb);
        let gdb = GraphDatabase::TigerGraph;
        assert_eq!(GraphDatabase::TigerGraph, gdb);

        let default_gdb = GraphDatabase::default();
        assert_eq!(GraphDatabase::Neo4J, default_gdb);
    }
}
